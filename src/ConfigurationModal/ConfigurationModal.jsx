import React from "react";
import { listAll } from "../utils/variables";
import { groupFields } from "../utils/groupSlides";
import "./ConfigurationModal.css";

class ConfigurationModal extends React.Component {
  handleClick = () => {
    this.props.toggleShowGame();
  };
  render() {
    return (
      <div className='configuration-modal'>
        <div className='configuration-modal__header'>Choisissez la configuration</div>
        <div className='configuration-modal__list configuration-modal__list-all-cases'>
          {this.props.configuration.map((item) => {
            return (
              <div className='configuration-modal__list-item' key={item}>
                <img src={item} alt='cats' width='70' height='70' />
              </div>
            );
          })}
        </div>
        <div className='configuration-modal__separator'>
          <div>Tous les cas: </div>
          <hr />
        </div>
        <div className='configuration-modal__field-group'>
          {groupFields(listAll).map((col) => {
            return (
              <div key={col}>
                {col.map((item, index) => (
                  <div
                    className='configuration-modal__field'
                    onClick={() => this.props.setConfiguration(item.name)}
                    key={item + index.toString()}
                  >
                    <img
                      src={item.name}
                      alt={item.alt}
                      width='70'
                      height='70'
                    />
                  </div>
                ))}
              </div>
            );
          })}
        </div>
        <div className='configuration-modal__footer'>
          <button
            className='configuration-modal__button'
            onClick={this.handleClick}
          >
            Confirmer le choix
          </button>
          <button
            className='configuration-modal__button-clear'
            onClick={this.props.clearTable}
          >
            Supprimer la liste
          </button>
        </div>
      </div>
    );
  }
}

export default ConfigurationModal;
