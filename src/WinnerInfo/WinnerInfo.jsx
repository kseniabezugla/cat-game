import React from "react";
import "./WinnerInfo.css"; // Импортируем файл стилей

class WinnerInfo extends React.Component {
  handleClick = () => {
    this.props.resetGame();
    this.props.toggleShowGame();
    this.props.clearConfiguration();
  };
  render() {
    const { nbOfGuesses } = this.props;
    return (
      <div className='winner-info-container'>
        <div className='winner-text'>Winner!</div>
        <div className='guesses-text'>Nombre de tentatives:{nbOfGuesses}</div>
        <div className='restart-text' onClick={this.handleClick}>
          Recommencer la partie
        </div>
      </div>
    );
  }
}

export default WinnerInfo;
