import ConfigurationModal from "../ConfigurationModal/ConfigurationModal";
import "./App.css";
import React from "react";
import GamePage from "../GamePage/GamePage";
import { nbCol } from "../utils/variables";

class App extends React.Component {
  state = {
    configuration: [],
    isGameShown: false,
  };
  setConfiguration = (name) => {
    if (this.state.configuration.length < nbCol) {
      return this.setState((state) => ({
        configuration: [...state.configuration, name],
      }));
    }
  };
  clearConfiguration = () => {
    return this.setState({
      configuration: [],
    });
  };
  toggleShowGame = () => {
    if (this.state.configuration.length === nbCol) {
      return this.setState((state) => ({
        isGameShown: !state.isGameShown,
      }));
    }
  };
  render() {
    const { isGameShown } = this.state;
    return (
      <>
        {isGameShown ? (
          <GamePage
            configuration={this.state.configuration}
            toggleShowGame={this.toggleShowGame}
            clearConfiguration={this.clearConfiguration}
          />
        ) : (
          <ConfigurationModal
            configuration={this.state.configuration}
            setConfiguration={this.setConfiguration}
            clearTable={this.clearConfiguration}
            toggleShowGame={this.toggleShowGame}
            isGameShown={isGameShown}
          />
        )}
      </>
    );
  }
}

export default App;
