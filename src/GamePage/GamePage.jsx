import React from "react";
import { listAll, nbRows, nbCol } from "../utils/variables";
import { groupFields } from "../utils/groupSlides";
import { intersection, fill } from "lodash";
import { getMatchingIndices } from "../utils/functions";
import "./GamePage.css";
import WinnerInfo from "../WinnerInfo/WinnerInfo";

class GamePage extends React.Component {
  state = {
    wordArray:Array(nbRows * nbCol).fill(" "),
    nbOfGuesses: 0,
    currentGuess: [],
    submitRowBtn: false,
    intersectionRow: [],
    indexMatch: [],
    errorSubmit: false,
    winner: false,
  };
  setCurrentGuess = (event) => {
    const url = event.target.src;
    const urlGuess =
      url && url.includes("localhost")
        ? url.replace(/^.*?:\/\/.*?(\/static.*)$/, "$1")
        : url;
    if (this.state.currentGuess.length < nbCol) {
      return this.setState((state) => ({
        currentGuess: [...state.currentGuess, urlGuess],
      }));
    }
  };
  setWordRow = (event) => {
    const url = event.target.src;
    const urlRow =
      url && url.includes("localhost")
        ? url.replace(/^.*?:\/\/.*?(\/static.*)$/, "$1")
        : url;
    if (this.state.currentGuess.length !== nbCol) {
      const newArr = [...this.state.wordArray];
      const index = newArr.findIndex((el) => el === " ");
      if (index !== -1) {
        newArr.splice(index, 1, urlRow);
      }
      this.setState({
        wordArray: newArr,
      });
    }
  };
  clearCurrentRow = () => {
    return this.setState((state) => ({
      currentGuess: [],
      wordArray: [
        ...fill(
          state.wordArray,
          " ",
          this.state.nbOfGuesses * nbCol,
          (this.state.nbOfGuesses + 1) * nbCol
        ),
      ],
    }));
  };
  submitRow = () => {
    if (this.state.currentGuess.length === nbCol) {
      const matchingIndices = getMatchingIndices(
        this.state.currentGuess,
        this.props.configuration
      ).map((item) => item + nbCol * this.state.nbOfGuesses);
      this.setState((prevState) => ({
        indexMatch: [...prevState.indexMatch, ...matchingIndices],
        intersectionRow: intersection(
          this.state.currentGuess,
          this.props.configuration
        ),
        currentGuess: [],
        submitRowBtn: true,
        nbOfGuesses: prevState.nbOfGuesses + 1,
      }));
    }
    this.setState({
      errorSubmit: true,
    });
  };
  resetGame = () => {
    this.setState({
      wordArray: Array(nbRows * nbCol).fill(" "),
      nbOfGuesses: 0,
      currentGuess: [],
      submitRowBtn: false,
      intersectionRow: [],
      indexMatch: [],
      errorSubmit: false,
      winner: false,
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.nbOfGuesses !== prevState.nbOfGuesses) {
      if (this.state.intersectionRow.length === nbCol) {
        this.setState({ winner: true });
      }
      this.setState({
        currentGuess: [],
        submitRowBtn: false,
        errorSubmit: false,
      });
    }
  }
  render() {
    const {
      wordArray,
      submitRowBtn,
      intersectionRow,
      indexMatch,
      errorSubmit,
      nbOfGuesses,
      winner,
    } = this.state;
    return (
      <>
        {winner ? (
          <WinnerInfo
            nbOfGuesses={nbOfGuesses}
            resetGame={this.resetGame}
            clearConfiguration={this.props.clearConfiguration}
            toggleShowGame={this.props.toggleShowGame}
          />
        ) : (
          <div className='container'>
            <div className='game-board'>
              {groupFields(wordArray, nbCol).map((col, i) => {
                return (
                  <div key={i}>
                    {col.map((item, j) => {
                      const index = j * nbCol + i;
                      const isHighlightedGreen =
                        submitRowBtn && indexMatch.includes(index);
                      const isHighlightedOrange =
                        submitRowBtn && intersectionRow.includes(item);
                      return (
                        <div key={j}>
                          <img
                            key={j}
                            style={{
                              background: `${
                                (isHighlightedGreen && "green") ||
                                (isHighlightedOrange && "orange")
                              }`,
                            }}
                            className='word-cell'
                            src={item}
                            // alt={`cat${index}`}
                            width='70'
                            height='70'
                          />
                        </div>
                      );
                    })}
                  </div>
                );
              })}
            </div>
            <div className='game-board'>
              {groupFields(listAll).map((col) => {
                return (
                  <div key={col}>
                    {col.map((item, index) => (
                      <div
                        onClick={(e) => {
                          this.setWordRow(e);
                          this.setCurrentGuess(e);
                        }}
                        key={item + index.toString()}
                        className='word-option'
                      >
                        <img
                          src={item.name}
                          alt={item.alt}
                          width='70'
                          height='70'
                        />
                      </div>
                    ))}
                  </div>
                );
              })}
            </div>
            <div style={{ display: "flex", marginTop: "10px" }}>
              <div onClick={this.submitRow} className='gamepage_button'>
                Submit row
              </div>
              <div
                onClick={this.clearCurrentRow}
                className='gamepage_button-clear'
              >
                Clear row
              </div>
            </div>

            {errorSubmit && (
              <div style={{ color: "red" }}>Moins de 6 suppositions</div>
            )}
          </div>
        )}
      </>
    );
  }
}

export default GamePage;
