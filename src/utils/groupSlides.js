import { chunk, filter, zip } from "lodash";
export const groupFields = (field, colNumber = 5) => {
  const chunkArray = field && chunk(field, colNumber);
  return zip(...chunkArray).map((item) => filter(item));
};
