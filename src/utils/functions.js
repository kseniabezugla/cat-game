export const getMatchingIndices = (arr1, arr2) => {
  return arr1
    .map((val, i) => {
      if (val === arr2[i]) {
        return i;
      }
      return undefined;
    })
    .filter((val) => val !== undefined);
};
