import cat1 from "../assets/cat1.jpg";
import cat2 from "../assets/cat2.jpg";
import cat3 from "../assets/cat3.jpg";
import cat4 from "../assets/cat4.jpg";
import cat5 from "../assets/cat5.jpg";
import cat6 from "../assets/cat6.jpg";
import cat7 from "../assets/cat7.jpg";
import cat8 from "../assets/cat8.jpg";
import cat9 from "../assets/cat9.jpg";
import cat10 from "../assets/cat10.jpg";

export const nbRows = 5;
export const nbCol = 6;
export const listAll = [
  { name: cat1, alt: "cat1" },
  { name: cat2, alt: "cat2" },
  { name: cat3, alt: "cat3" },
  { name: cat4, alt: "cat4" },
  { name: cat5, alt: "cat5" },
  { name: cat6, alt: "cat6" },
  { name: cat7, alt: "cat7" },
  { name: cat8, alt: "cat8" },
  { name: cat9, alt: "cat9" },
  { name: cat10, alt: "cat10" },
];
